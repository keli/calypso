/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
   */

///////////////////////////////////////////////////////////////////
// TrackerCluster.cxx
//   Implementation file for class TrackerCluster
///////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////
// Version 1.0 15/07/2003 Veronique Boisvert
///////////////////////////////////////////////////////////////////

#include "TrackerRawData/TrackerCluster.h"
#include "GaudiKernel/MsgStream.h"


// Constructor for EF:
TrackerCluster::TrackerCluster(
    const Identifier &RDOId,
    const Amg::Vector2D& locpos, 
    const std::vector<Identifier>& rdoList, 
    const FaserSiWidth& width,
    const TrackerDD::SiDetectorElement* detEl,
    const Amg::MatrixX* locErrMat
    ) :
  m_clusId(RDOId),
  m_localPos(locpos),
  m_rdoList(rdoList),
  m_localCovariance(locErrMat),
  m_indexAndHash(),
  m_width(width),
  m_globalPosition{},
  m_gangedPixel(0),
  m_detEl(detEl) {}

  TrackerCluster::TrackerCluster(
      const Identifier &RDOId,
      const Amg::Vector2D& locpos, 
      std::vector<Identifier>&& rdoList, 
      const FaserSiWidth& width,
      const TrackerDD::SiDetectorElement* detEl,
      std::unique_ptr<const Amg::MatrixX> locErrMat
      ) :
    m_clusId(RDOId),
    m_localPos(locpos),
    m_rdoList(std::move(rdoList)),
    m_localCovariance(locErrMat.release()),
    m_indexAndHash(),
    m_width(width),
    m_globalPosition{},
    m_gangedPixel(0),
    m_detEl(detEl) {}

    // Destructor:
TrackerCluster::~TrackerCluster()
{
  // do not delete m_detEl since owned by DetectorStore
}

// Default constructor:
TrackerCluster::TrackerCluster():
  m_clusId(0), 
  m_localPos(),
  m_rdoList(),  
  m_localCovariance(0),
  m_indexAndHash(),
  m_globalPosition{},
  m_gangedPixel(0),
  m_detEl(0)
{}

//copy constructor:
TrackerCluster::TrackerCluster(const TrackerCluster& RIO):
  m_clusId(RIO.m_clusId), 
  m_localPos( RIO.m_localPos ),
  m_rdoList(RIO.m_rdoList),  
  m_localCovariance( RIO.m_localCovariance ? new Amg::MatrixX(*RIO.m_localCovariance) : 0 ),
  m_indexAndHash(RIO.m_indexAndHash),
  m_width( RIO.m_width ),
  m_globalPosition{},
  m_gangedPixel( RIO.m_gangedPixel ),
  m_detEl( RIO.m_detEl )

{
  // copy only if it exists
  if (RIO.m_globalPosition) {
    m_globalPosition.set(std::make_unique<Amg::Vector3D>(*RIO.m_globalPosition));
  }
}

//move constructor:
TrackerCluster::TrackerCluster(TrackerCluster&& RIO):
  m_clusId(RIO.m_clusId), 
  m_localPos( RIO.m_localPos ),
  m_rdoList(std::move(RIO.m_rdoList)),  
  m_localCovariance( RIO.m_localCovariance ),
  m_indexAndHash(RIO.m_indexAndHash),
  m_width( std::move(RIO.m_width) ),
  m_globalPosition( std::move(RIO.m_globalPosition) ),
  m_gangedPixel( RIO.m_gangedPixel ),
  m_detEl( RIO.m_detEl )

{
}

//assignment operator
TrackerCluster& TrackerCluster::operator=(const TrackerCluster& RIO){
  if (&RIO !=this) {
    m_clusId = RIO.m_clusId;
    m_rdoList = RIO.m_rdoList;
    m_localPos = RIO.m_localPos;
    delete m_localCovariance;
    m_localCovariance = RIO.m_localCovariance ? new Amg::MatrixX(*RIO.m_localCovariance) : 0;
    m_indexAndHash = RIO.m_indexAndHash;

    m_width = RIO.m_width;
    if (RIO.m_globalPosition) {
      m_globalPosition.set(std::make_unique<Amg::Vector3D>(*RIO.m_globalPosition));
    } else if (m_globalPosition) {
      m_globalPosition.release().reset();
    }
    m_gangedPixel = RIO.m_gangedPixel;
    m_detEl =  RIO.m_detEl ;
  }
  return *this;
} 

//move operator
TrackerCluster& TrackerCluster::operator=(TrackerCluster&& RIO){
  if (&RIO !=this) {
    m_clusId = std::move(RIO.m_clusId);
    m_rdoList = std::move(RIO.m_rdoList);
    m_localPos = std::move(RIO.m_localPos);
    delete m_localCovariance;
    m_localCovariance = RIO.m_localCovariance;
    RIO.m_localCovariance = nullptr;
    m_indexAndHash = RIO.m_indexAndHash;

    m_width = RIO.m_width;
    m_globalPosition = std::move(RIO.m_globalPosition);
    m_gangedPixel = RIO.m_gangedPixel;
    m_detEl =  RIO.m_detEl ;
  }
  return *this;
} 

MsgStream& TrackerCluster::dump( MsgStream&    stream) const
{
  stream << "TrackerCluster object"<<std::endl;

  // have to do a lot of annoying checking to make sure that PRD is valid. 
  {
    stream << "at global coordinates (x,y,z) = ("<<this->globalPosition().x()<<", "
      <<this->globalPosition().y()<<", "
      <<this->globalPosition().z()<<")"<<std::endl;
  }

  if ( gangedPixel()==true ) 
  {
    stream << "and is a ganged pixel. "<<std::endl;
  } else {
    stream << "and is not a ganged pixel. "<<std::endl;
  }

  stream << "FaserSiWidth: " << m_width << std::endl;

  stream << "Base class (TrackerCluster):" << std::endl;
  this->TrackerCluster::dump(stream);

  return stream;
}

std::ostream& TrackerCluster::dump( std::ostream&    stream) const
{
  stream << "TrackerCluster object"<<std::endl;
  {
    stream << "at global coordinates (x,y,z) = ("<<this->globalPosition().x()<<", "
      <<this->globalPosition().y()<<", "
      <<this->globalPosition().z()<<")"<<std::endl;
  }

  if ( gangedPixel()==true ) 
  {
    stream << "and is a ganged pixel. "<<std::endl;
  } else {
    stream << "and is not a ganged pixel. "<<std::endl;
  }

  stream << "FaserSiWidth: " << m_width << std::endl;

  stream << "Base Class (TrackerCluster): " << std::endl;
  this->TrackerCluster::dump(stream);

  return stream;
}


MsgStream&    operator << (MsgStream& stream,    const TrackerCluster& prd)
{
  return prd.dump(stream);
}

std::ostream& operator << (std::ostream& stream, const TrackerCluster& prd)
{
  return prd.dump(stream);
}

