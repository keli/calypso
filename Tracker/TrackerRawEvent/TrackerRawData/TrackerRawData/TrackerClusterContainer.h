/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// TrackerClusterContainer.h
//   Header file for class TrackerClusterContainer
///////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#ifndef TRACKERRAWDATA_TRACKERCLUSTERCONTAINER_H
#define TRACKERRAWDATA_TRACKERCLUSTERCONTAINER_H

// Base classes
#include "EventContainers/IdentifiableContainer.h"


template<class CollectionT>
class TrackerClusterContainer 
    : public IdentifiableContainer<CollectionT>{
    
    ///////////////////////////////////////////////////////////////////
    // Public methods:
    ///////////////////////////////////////////////////////////////////
    public:
    
    /** Constructor with parameters:*/
    TrackerClusterContainer(unsigned int max);
    
    /** Constructor with External Cache*/
    TrackerClusterContainer(EventContainers::IdentifiableCache<CollectionT>*);

    /**default ctor - for POOL only*/
    TrackerClusterContainer();
    
    /** Destructor:*/
    virtual ~TrackerClusterContainer();
    
    /** return class ID */
    static const CLID& classID();
    
    /** return class ID */
    virtual const CLID& clID() const;
    
    private:
    
    TrackerClusterContainer(const TrackerClusterContainer&);
    TrackerClusterContainer &operator=(const TrackerClusterContainer&);

};
// member functions that use Collection T
#include"TrackerRawData/TrackerClusterContainer.icc"


#endif // TRACKERRAWDATA_TRACKERCLUSTERCONTAINER_H

