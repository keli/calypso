/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// PrepRawDataCLASS_DEF.h
//   Header file for class PrepRawDataCLASS_DEF
///////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////
// Class to contain all the CLASS_DEF for Containers and Collections
///////////////////////////////////////////////////////////////////
// Version 1.0 25/09/2002 Veronique Boisvert
///////////////////////////////////////////////////////////////////

#ifndef FASERSCT_CLUSTERCOLLECTION_H
#define FASERSCT_CLUSTERCOLLECTION_H

#include "AthenaKernel/CLASS_DEF.h"
#include "TrackerRawData/TrackerClusterCollection.h"
#include "TrackerRawData/FaserSCT_Cluster.h"
#include "TrackerRawData/TrackerRawDataCollection.h"

// Containers
// numbers obtained using clid.db
typedef TrackerRawDataCollection< FaserSCT_Cluster > FaserSCT_ClusterCollection;

/**Overload of << operator for MsgStream for debug output*/
MsgStream& operator << ( MsgStream& sl, const FaserSCT_ClusterCollection& coll);

/**Overload of << operator for std::ostream for debug output*/ 
std::ostream& operator << ( std::ostream& sl, const FaserSCT_ClusterCollection& coll);

CLASS_DEF(FaserSCT_ClusterCollection, 1240147234, 1)

#endif // TRKPREPRAWDATA_PREPRAWDATACLASS_DEF_H
