/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// TrackerCluster.h
//   Header file for class TrackerCluster
///////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////
// Class to implement Cluster for Tracker
///////////////////////////////////////////////////////////////////
// Version 1.0 15/07/2003 Veronique Boisvert
///////////////////////////////////////////////////////////////////

#ifndef TRACKERRAWDATA_SICLUSTER_H
#define TRACKERRAWDATA_SICLUSTER_H

#include "CxxUtils/CachedUniquePtr.h"
#include "Identifier/Identifier.h"
#include "TrackerRawData/FaserSiWidth.h"
#include "TrackerReadoutGeometry/SiDetectorElement.h" // cant be forward declared
#include "AthLinks/tools/IdentContIndex.h"

#include "TrkSurfaces/Surface.h"

#include <memory>

class MsgStream;

class TrackerCluster  {

	///////////////////////////////////////////////////////////////////
	// Public methods:
	///////////////////////////////////////////////////////////////////
	public:
	
	/** Public, Copy, operator=, constructor*/
	TrackerCluster();
	TrackerCluster(const TrackerCluster &);
	TrackerCluster(TrackerCluster &&);
	TrackerCluster &operator=(const TrackerCluster &);
	TrackerCluster &operator=(TrackerCluster &&);
	
        /** Only constructor for TrackerCluster.
            Last datamember might not be always filled and will be 0 by default.
            The others including SiDetectorElement have to be given!
        */
        TrackerCluster( 
                  const Identifier& RDOId,
                  const Amg::Vector2D& locpos, 
                  const std::vector<Identifier>& rdoList,
                  const FaserSiWidth& width,
                  const TrackerDD::SiDetectorElement* detEl,
                  const Amg::MatrixX* locErrMat = 0
                 );

        TrackerCluster( 
                  const Identifier& RDOId,
                  const Amg::Vector2D& locpos, 
                  std::vector<Identifier>&& rdoList,
                  const FaserSiWidth& width,
                  const TrackerDD::SiDetectorElement* detEl,
                  std::unique_ptr<const Amg::MatrixX> locErrMat
                 );

        // Destructor:
	virtual ~TrackerCluster();
	
	///////////////////////////////////////////////////////////////////
	// Virtual methods 
	///////////////////////////////////////////////////////////////////
	
	/** return width class reference */
	virtual const FaserSiWidth&  width()  const;
	
	/** return global position reference */
	virtual const Amg::Vector3D& globalPosition() const;
	
	/** set the flag of this cluster containing a gangedPixel */
	virtual void setGangedPixel(bool ganged);
	
	/** return the flag of this cluster containing a gangedPixel */
	virtual bool gangedPixel() const;
	
	/** return the detector element corresponding to this PRD
	The pointer will be zero if the det el is not defined (i.e. it was not passed in by the ctor)*/
	virtual const TrackerDD::SiDetectorElement* detectorElement() const;
	//
	/* return the identifier*/
	Identifier identify() const;
	
	/** return the local position reference*/
	const Amg::Vector2D& localPosition() const;
	
	/** return the List of rdo identifiers (pointers) */
	const std::vector<Identifier>& rdoList() const;
	 
	/** return the error matrix reference */
	const Amg::MatrixX& localCovariance() const;
	
	/** TEMP for testing: might make some classes friends later ... */
	void setHashAndIndex(unsigned short collHash, unsigned short objIndex);
	const IdentContIndex& getHashAndIndex() const;

	/** dump information about the SiCluster*/
	virtual MsgStream&    dump( MsgStream&    stream) const;
	/** dump information about the SiCluster*/
	virtual std::ostream& dump( std::ostream& stream) const;

	private:
	FaserSiWidth m_width; //col, row, and width in mm
	CxxUtils::CachedUniquePtr<const Amg::Vector3D> m_globalPosition;
	bool m_gangedPixel;
	const TrackerDD::SiDetectorElement* m_detEl;
	Identifier m_clusId; 
	/**see derived classes for definition of meaning of LocalPosition*/
	// Need to force proper alignment; otherwise cling gets it wrong.
	alignas(16) Amg::Vector2D m_localPos;
	/**Stores the identifiers of the RDOs.*/
	std::vector<Identifier> m_rdoList; 
	/**See derived classes for definition of ErrorMatrix */
	const Amg::MatrixX* m_localCovariance;

	/**Stores its own position (index) in collection plus the hash id for the collection 
	  (needed for the EL to IDC) */
	IdentContIndex m_indexAndHash;

};

 MsgStream&    operator << (MsgStream& stream,    const TrackerCluster& prd);
 std::ostream& operator << (std::ostream& stream, const TrackerCluster& prd);


///////////////////////////////////////////////////////////////////
// Inline methods:
///////////////////////////////////////////////////////////////////
inline Identifier TrackerCluster::identify() const
{
    return m_clusId;
}

inline const Amg::Vector2D& TrackerCluster::localPosition() const
{
    return m_localPos;
}

inline const Amg::MatrixX& TrackerCluster::localCovariance() const
{
    return *m_localCovariance;
}

inline  const std::vector<Identifier>& TrackerCluster::rdoList() const
{
    return m_rdoList;
}
//
/** TEMP for testing: might make some classes friends later ... */
inline void TrackerCluster::setHashAndIndex(unsigned short collHash, unsigned short objIndex)
{
  m_indexAndHash.setCollHash(collHash);
  m_indexAndHash.setObjIndex(objIndex);
}
inline const IdentContIndex& TrackerCluster::getHashAndIndex() const
{
  return m_indexAndHash;
}

// return width:
inline const FaserSiWidth&  TrackerCluster::width() const
{
	return m_width;
}
// return globalPosition:
inline const Amg::Vector3D& TrackerCluster::globalPosition() const
{
  if (not m_globalPosition) {
    m_globalPosition.set(std::unique_ptr<const Amg::Vector3D>(m_detEl->surface(identify()).localToGlobal(localPosition())));
  }
  return *m_globalPosition;
}
// set gangedPixel:
inline void TrackerCluster::setGangedPixel(bool ganged){
	m_gangedPixel = ganged;
}
// get gangedPixel:
inline bool TrackerCluster::gangedPixel() const
{
	return m_gangedPixel;
}

inline const TrackerDD::SiDetectorElement* TrackerCluster::detectorElement() const
{
	return m_detEl;
}



#endif // TRACKERRAWDATA_SICLUSTER_H
