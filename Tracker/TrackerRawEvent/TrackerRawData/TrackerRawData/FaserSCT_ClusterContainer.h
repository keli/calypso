/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// PrepRawDataCLASS_DEF.h
//   Header file for class PrepRawDataCLASS_DEF
///////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////
// Class to contain all the CLASS_DEF for Containers and Collections
///////////////////////////////////////////////////////////////////
// Version 1.0 25/09/2002 Veronique Boisvert
///////////////////////////////////////////////////////////////////

#ifndef SCT_CLUSTERCONTAINER_H
#define SCT_CLUSTERCONTAINER_H

#include "AthenaKernel/CLASS_DEF.h"
#include "TrackerRawData/TrackerClusterContainer.h"
#include "TrackerRawData/FaserSCT_ClusterCollection.h"
#include "TrackerRawData/TrackerRawDataContainer.h"
#include "AthLinks/DeclareIndexingPolicy.h"

// Containers
// numbers obtained using clid.db
typedef TrackerRawDataContainer< FaserSCT_ClusterCollection > FaserSCT_ClusterContainer;
//typedef TrackerClusterContainer< FaserSCT_ClusterCollection > FaserSCT_ClusterContainer;
typedef EventContainers::IdentifiableCache< FaserSCT_ClusterCollection > FaserSCT_ClusterContainerCache;

CLASS_DEF(FaserSCT_ClusterContainer,1301915248,1)
CONTAINER_IS_IDENTCONT(FaserSCT_ClusterContainer)

CLASS_DEF( FaserSCT_ClusterContainerCache , 1284702459, 1 )

#endif // TRKPREPRAWDATA_PREPRAWDATACLASS_DEF_H
