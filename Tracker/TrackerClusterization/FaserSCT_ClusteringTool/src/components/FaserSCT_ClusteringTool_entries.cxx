#include "FaserSCT_ClusteringTool/FaserSCT_ClusteringTool.h"
#include "FaserSCT_ClusteringTool/FaserSCT_ReClustering.h"
#include "FaserSCT_ClusteringTool/TrackerClusterMakerTool.h"

DECLARE_COMPONENT( FaserSCT_ClusteringTool )
DECLARE_COMPONENT( TrackerClusterMakerTool )
