# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

from AthenaCommon.CfgGetter import addTool,addService,addAlgorithm
addTool("FaserSCT_ClusteringTool.FaserSCT_ClusteringToolConf.FaserSCT_ClusteringTool", "FaserSCT_ClusteringTool")
