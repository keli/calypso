/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
   */


/**
 * @file FaserCacheCreator.h
 */

#ifndef TRACKERCLUSTERIZATION_FASERCACHECREATOR
#define TRACKERCLUSTERIZATION_FASERCACHECREATOR

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "TrackerRawData/FaserSCT_ClusterContainer.h"
#include "TrackerRawData/FaserSCT_RDO_Container.h"

#include "AthenaPoolUtilities/CondAttrListCollection.h"

#include <atomic>

class FaserSCT_ID;

class FaserCacheCreator : public AthReentrantAlgorithm
{
  public:

    FaserCacheCreator(const std::string &name,ISvcLocator *pSvcLocator);
    virtual ~FaserCacheCreator()  ;
    virtual StatusCode initialize () override;
    virtual StatusCode execute (const EventContext& ctx) const override;
    //No need for finalize
  protected:
    const FaserSCT_ID*  m_sct_idHelper;
    SG::WriteHandleKey<FaserSCT_ClusterContainerCache>            m_SCTclusterContainerCacheKey
    {this, "FaserSCT_ClusterCacheKey", ""};
    //        SG::WriteHandleKey<SpacePointCache>    m_SCTSpacePointCacheKey
    //          {this, "SpacePointCacheSCT", ""};
    SG::WriteHandleKey<FaserSCT_RDO_Cache>      m_SCTRDOCacheKey
    {this, "SCTRDOCacheKey", ""};
    //	SG::WriteHandleKey<IDCInDetBSErrContainer_Cache> m_SCTBSErrCacheKey
    //	  {this, "SCTBSErrCacheKey", ""};
    BooleanProperty m_disableWarning{this, "DisableViewWarning", false};
    mutable std::atomic_bool m_disableWarningCheck;
    //Temporary workarounds for problem in scheduler - remove later
    bool isInsideView(const EventContext&) const;
    template<typename T>
      StatusCode createContainer(const SG::WriteHandleKey<T>& , long unsigned int , const EventContext& ) const;
};

template<typename T>
StatusCode FaserCacheCreator::createContainer(const SG::WriteHandleKey<T>& containerKey, long unsigned int size, const EventContext& ctx) const{
  if(containerKey.key().empty()){
    ATH_MSG_DEBUG( "Creation of container "<< containerKey.key() << " is disabled (no name specified)");
    return StatusCode::SUCCESS;
  }
  SG::WriteHandle<T> ContainerCacheKey(containerKey, ctx);
  ATH_CHECK( ContainerCacheKey.recordNonConst ( std::make_unique<T>(IdentifierHash(size), nullptr) ));
  ATH_MSG_DEBUG( "Container "<< containerKey.key() << " created to hold " << size );
  return StatusCode::SUCCESS;
}

#endif //INDETPREPRAWDATAFORMATION_CACHECREATOR
