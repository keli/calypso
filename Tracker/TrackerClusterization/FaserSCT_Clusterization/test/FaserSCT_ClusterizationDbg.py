#!/usr/bin/env python
"""Test various ComponentAccumulator Digitization configuration modules

Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
"""
import sys
from AthenaCommon.Logging import log, logging
from AthenaCommon.Constants import DEBUG, VERBOSE, INFO
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from AthenaConfiguration.TestDefaults import defaultTestFiles
from AthenaConfiguration.MainServicesConfig import MainServicesSerialCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
#from Digitization.DigitizationParametersConfig import writeDigitizationMetadata
from FaserSCT_Clusterization.FaserSCT_ClusterizationConfig import FaserSCT_ClusterizationCfg
#from MCTruthSimAlgs.RecoTimingConfig import MergeRecoTimingObjCfg

# Set up logging and new style config
log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

# Configure
ConfigFlags.Input.Files = ['myRDO.pool.root']
ConfigFlags.Output.RDOFileName = "myRDO_cluster.pool.root"
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-XXXX-XXX-XX"
ConfigFlags.GeoModel.Align.Dynamic = False
ConfigFlags.Concurrency.NumThreads = 1
ConfigFlags.Beam.NumberOfCollisions = 0.
#ConfigFlags.Detector.SimulateFaserSCT = True

ConfigFlags.GeoModel.FaserVersion = "FASER-00"               # Always needed
ConfigFlags.GeoModel.AtlasVersion = "ATLAS-R2-2016-01-00-01" # Always needed to fool autoconfig; value ignored

ConfigFlags.lock()

# Core components
acc = MainServicesSerialCfg()
acc.merge(PoolReadCfg(ConfigFlags))
acc.merge(PoolWriteCfg(ConfigFlags))

#acc.merge(writeDigitizationMetadata(ConfigFlags))

# Inner Detector
acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags))

# Output Stream customization
oStream = acc.getEventAlgo("OutputStreamRDO")
oStream.ItemList += ["EventInfo#*",
                     "McEventCollection#TruthEvent",
                     "McEventCollection#GEN_EVENT",
                     "FaserSCT_ClusterCollection#*"
                    ]
                    
# Timing
#acc.merge(MergeRecoTimingObjCfg(ConfigFlags))

# Dump config
logging.getLogger('forcomps').setLevel(VERBOSE)
acc.foreach_component("*").OutputLevel = VERBOSE
acc.foreach_component("*ClassID*").OutputLevel = INFO
acc.getCondAlgo("FaserSCT_AlignCondAlg").OutputLevel = VERBOSE
acc.getCondAlgo("FaserSCT_DetectorElementCondAlg").OutputLevel = VERBOSE
acc.getService("StoreGateSvc").Dump = True
acc.getService("ConditionStore").Dump = True
acc.printConfig(withDetails=True)
ConfigFlags.dump()
# Execute and finish
sc = acc.run(maxEvents=-1)
# Success should be 0
sys.exit(not sc.isSuccess())
