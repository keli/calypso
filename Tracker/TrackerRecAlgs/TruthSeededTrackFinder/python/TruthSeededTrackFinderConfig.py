"""Define methods to construct configured SCT Digitization tools and algorithms

Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
"""
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from TruthSeededTrackFinder.TruthSeededTrackFinderConf import Tracker__TruthSeededTrackFinder
PileUpXingFolder=CompFactory.PileUpXingFolder

from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
from GaudiSvc.GaudiSvcConf import THistSvc
histSvc= THistSvc()
histSvc.Output += [ "TruthTrackSeeds DATAFILE='truthtrackseeds.root' OPT='RECREATE'" ]


def TruthSeededTrackFinderBasicCfg(flags, **kwargs):
    """Return ComponentAccumulator for TruthSeededTrackFinder"""
    acc = ComponentAccumulator()
    kwargs.setdefault("SpacePointsSCTName", "SCT_SpacePointContainer")
    kwargs.setdefault("SpacePointsSeedsName", "Seeds_SpacePointContainer")
    acc.addEventAlgo(Tracker__TruthSeededTrackFinder(**kwargs))
    return acc

def TruthSeededTrackFinder_OutputCfg(flags):
    """Return ComponentAccumulator with Output for SCT. Not standalone."""
    acc = ComponentAccumulator()
    #ItemList = ["FaserSCT_ClusterContainer#*"]
    acc.merge(OutputStreamCfg(flags, "RDO"))
    return acc

def TruthSeededTrackFinderCfg(flags, **kwargs):
    #acc=ComponentAccumulator(flags)
    acc=TruthSeededTrackFinderBasicCfg(flags, **kwargs)
    acc.addService(histSvc)
    acc.merge(TruthSeededTrackFinder_OutputCfg(flags))
    return acc
