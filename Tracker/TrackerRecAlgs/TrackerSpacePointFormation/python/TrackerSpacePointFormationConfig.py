"""Define methods to construct configured SCT Digitization tools and algorithms

Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
"""
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from TrackerSpacePointFormation.TrackerSpacePointFormationConf import Tracker__TrackerSpacePointFinder, Tracker__StatisticsAlg, TrackerDD__SiElementPropertiesTableCondAlg
PileUpXingFolder=CompFactory.PileUpXingFolder

from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
#SCT_RadDamageSummaryTool=CompFactory.SCT_RadDamageSummaryTool
from FaserGeoModel.SCTGMConfig import SctGeometryCfg

#from FaserSCT_ConditionsTools.FaserSCT_DCSConditionsConfig import FaserSCT_DCSConditionsCfg
#from FaserSCT_ConditionsTools.FaserSCT_DCSConditionsConfig import FaserSCT_DCSConditionsToolCfg
from AthenaCommon.AlgSequence import AthSequencer
condSeq = AthSequencer("AthCondSeq")
condSeq += TrackerDD__SiElementPropertiesTableCondAlg(name = "SiElementPropertiesTableCondAlg")
from GaudiSvc.GaudiSvcConf import THistSvc
histSvc= THistSvc()
histSvc.Output += [ "StatisticsAlg DATAFILE='statistics.root' OPT='RECREATE'" ]



def TrackerDDSiElementPropertiesTableCondAlgCfg(flags, **kwargs):
  acc = ComponentAccumulator()
  acc.addCondAlgo(CompFactory.TrackerDD__SiElementPropertiesTableCondAlg(name = "TrackerDDSiElementPropertiesTableCondAlg", **kwargs))
  return acc


def TrackerSpacePointMakerCommonCfg(flags, name="TrackerSpacePointMakerTool", **kwargs):
    """Return ComponentAccumulator with common FaserSCT Clusterization tool config"""
    acc = SctGeometryCfg(flags)

    kwargs.setdefault("InputObjectName", "SCT_ClusterContainer")
    from FaserSiSpacePointTool.FaserSiSpacePointToolConf import Tracker__TrackerSpacePointMakerTool
#    fieldSvc = acc.getService("FaserFieldSvc")
    trackerSpacePointMakerTool = Tracker__TrackerSpacePointMakerTool(name)
    # attach ToolHandles
    acc.setPrivateTools(trackerSpacePointMakerTool)
    return acc


def TrackerSpacePointMakerToolCfg(flags, name="TrackerSpacePointMakerTool", **kwargs):
    """Return ComponentAccumulator with configured TrackerSpacePointMakerTool"""
    #kwargs.setdefault("HardScatterSplittingMode", 0)
    return TrackerSpacePointMakerCommonCfg(flags, name, **kwargs)


def TrackerSpacePointFinderBasicCfg(flags, **kwargs):
    """Return ComponentAccumulator for TrackerSpacePointFinder"""
    acc = ComponentAccumulator()
    finderTool = acc.popToolsAndMerge(TrackerSpacePointMakerToolCfg(flags))
    kwargs.setdefault("SpacePointFinderTool", finderTool)
    kwargs.setdefault("SCT_ClustersName", "SCT_ClusterContainer")
    kwargs.setdefault("SpacePointsSCTName", "SCT_SpacePointContainer")
    kwargs.setdefault("SpacePointsOverlapName", "SCT_SpacePointOverlapCollection")
    # kwargs.setdefault("SCT_FlaggedCondData", "SCT_Flags");
    acc.addEventAlgo(Tracker__TrackerSpacePointFinder(**kwargs))
    return acc

def StatisticsBasicCfg(flags, **kwargs):
    """Return ComponentAccumulator for TrackerSpacePointMakerAlg"""
    acc = ComponentAccumulator()
    acc.popToolsAndMerge(TrackerSpacePointMakerToolCfg(flags))
    kwargs.setdefault("SpacePointsSCTName", "SCT_SpacePointContainer")
    acc.addEventAlgo(Tracker__StatisticsAlg(**kwargs))
    return acc

def TrackerSpacePointFinder_OutputCfg(flags):                                                                                                    
    """Return ComponentAccumulator with Output for SCT. Not standalone."""
    acc = ComponentAccumulator()
    # ItemList = ["SpacePointContainer#*"]
    ItemList = [""]
    acc.merge(OutputStreamCfg(flags, "RDO", ItemList))
    ostream = acc.getEventAlgo("OutputStreamRDO")
    ostream.TakeItemsFromInput=True
    return acc

# with output defaults
def TrackerSpacePointFinderCfg(flags, **kwargs):
    """Return ComponentAccumulator for SCT SpacePoints and Output"""
    acc=TrackerDDSiElementPropertiesTableCondAlgCfg(flags)
    acc.merge(TrackerSpacePointFinderBasicCfg(flags, **kwargs))
    acc.merge(TrackerSpacePointFinder_OutputCfg(flags))
    return acc

def StatisticsCfg(flags, **kwargs):
    #acc=ComponentAccumulator(flags)
    acc=StatisticsBasicCfg(flags, **kwargs)
    acc.addService(histSvc)
    acc.merge(TrackerSpacePointFinder_OutputCfg(flags))
    return acc
