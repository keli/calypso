/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "TrackerSimEventTPCnv/TrackerHits/FaserSiHitCollectionCnv_p1.h"
#include "TrackerSimEventTPCnv/TrackerHits/FaserSiHit_p1.h"
#include "FaserSiHitCollectionCnv.h"


FaserSiHitCollection_PERS* FaserSiHitCollectionCnv::createPersistent(FaserSiHitCollection* transCont) {
  MsgStream mlog(msgSvc(), "FaserSiHitCollectionConverter" );
  FaserSiHitCollectionCnv_PERS converter;
  FaserSiHitCollection_PERS *persObj = converter.createPersistent( transCont, mlog );
  return persObj;
}


FaserSiHitCollection* FaserSiHitCollectionCnv::createTransient() {
    MsgStream mlog(msgSvc(), "FaserSiHitCollectionConverter" );
    FaserSiHitCollectionCnv_p1   converter_p1;

    static const pool::Guid   p1_guid("FF9508DE-3E25-425D-9556-16D319DCE0E1");

    FaserSiHitCollection       *trans_cont(0);
    if( this->compareClassGuid(p1_guid)) {
      std::unique_ptr< FaserSiHitCollection_p1 >   col_vect( this->poolReadObject< FaserSiHitCollection_p1 >() );
      trans_cont = converter_p1.createTransient( col_vect.get(), mlog );
    }  else {
      throw std::runtime_error("Unsupported persistent version of Data container");
    }
    return trans_cont;
}
