"""Define methods to construct configured SCT Digitization tools and algorithms

Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
"""
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from TrackerSpacePointMakerAlg.TrackerSpacePointMakerAlgConf import TrackerSpacePointMakerAlg
from TrackerSpacePointMakerAlg.TrackerSpacePointMakerAlgConf import StatisticsAlg
PileUpXingFolder=CompFactory.PileUpXingFolder

#from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
#SCT_RadDamageSummaryTool=CompFactory.SCT_RadDamageSummaryTool
from FaserGeoModel.SCTGMConfig import SctGeometryCfg

#from FaserSCT_ConditionsTools.FaserSCT_DCSConditionsConfig import FaserSCT_DCSConditionsCfg
#from FaserSCT_ConditionsTools.FaserSCT_DCSConditionsConfig import FaserSCT_DCSConditionsToolCfg
from AthenaCommon.AlgSequence import AthSequencer
condSeq = AthSequencer("AthCondSeq")
from TrackerSpacePointMakerAlg.TrackerSpacePointMakerAlgConf import TrackerDD__SiElementPropertiesTableCondAlg
condSeq += TrackerDD__SiElementPropertiesTableCondAlg(name = "SiElementPropertiesTableCondAlg")
from GaudiSvc.GaudiSvcConf import THistSvc
histSvc= THistSvc()
histSvc.Output += [ "StatisticsAlg DATAFILE='statistics.root' OPT='RECREATE'" ]



def TrackerDDSiElementPropertiesTableCondAlgCfg(flags, **kwargs):
  acc = ComponentAccumulator()
  acc.addCondAlgo(CompFactory.TrackerDD__SiElementPropertiesTableCondAlg(name = "TrackerDDSiElementPropertiesTableCondAlg", **kwargs))
  return acc


def TrackerSpacePointMakerAlgCommonCfg(flags, name="TrackerSpacePointMakerAlg", **kwargs):
    """Return ComponentAccumulator with common FaserSCT Clusterization tool config"""
    acc = SctGeometryCfg(flags)

    kwargs.setdefault("InputObjectName", "SCT_ClusterContainer")
    from TrackerSpacePointMakerTool.TrackerSpacePointMakerToolConf import TrackerSpacePointMakerTool
#    fieldSvc = acc.getService("FaserFieldSvc")
    trackerSpacePointMakerTool = TrackerSpacePointMakerTool(name)
    # attach ToolHandles
    acc.setPrivateTools(trackerSpacePointMakerTool)
    return acc


def TrackerSpacePointMakerToolCfg(flags, name="TrackerrSpacePointMakerTool", **kwargs):
    """Return ComponentAccumulator with configured TrackerSpacePointMakerTool"""
    #kwargs.setdefault("HardScatterSplittingMode", 0)
    return TrackerSpacePointMakerAlgCommonCfg(flags, name, **kwargs)


def TrackerSpacePointMakerAlgBasicCfg(flags, **kwargs):
    """Return ComponentAccumulator for TrackerSpacePointMakerAlg"""
    acc = ComponentAccumulator()
    acc.popToolsAndMerge(TrackerSpacePointMakerToolCfg(flags))
    kwargs.setdefault("SCT_ClustersName", "SCT_ClusterContainer")
    kwargs.setdefault("SpacePointsSCTName", "SCT_SpacePointContainer")
    kwargs.setdefault("SpacePointsOverlapName", "SCT_SpacePointOverlapCollection")
    #kwargs.setdefault("SCT_FlaggedCondData", "SCT_Flags");
    acc.addEventAlgo(TrackerSpacePointMakerAlg(**kwargs))
    return acc
def StatisticsAlgBasicCfg(flags, **kwargs):
    """Return ComponentAccumulator for TrackerSpacePointMakerAlg"""
    acc = ComponentAccumulator()
    acc.popToolsAndMerge(TrackerSpacePointMakerToolCfg(flags))
    kwargs.setdefault("SpacePointsSCTName", "SCT_SpacePointContainer")
    acc.addEventAlgo(StatisticsAlg(**kwargs))
    return acc

def TrackerSpacePoint_OutputCfg(flags):                                                                                                    
    """Return ComponentAccumulator with Output for SCT. Not standalone."""
    acc = ComponentAccumulator()
#    ItemList = ["FaserSCT_ClusterContainer#*"]
#    acc.merge(OutputStreamCfg(flags, "RDO", ItemList))
    return acc



# with output defaults
def TrackerSpacePointMakerAlgCfg(flags, **kwargs):
    """Return ComponentAccumulator for SCT Clusterization and Output"""
    acc=TrackerDDSiElementPropertiesTableCondAlgCfg(flags)
#    acc = TrackerSpacePointMakerAlgBasicCfg(flags, **kwargs)
    acc.merge(TrackerSpacePointMakerAlgBasicCfg(flags, **kwargs))
    acc.merge(TrackerSpacePoint_OutputCfg(flags))
    return acc

def StatisticsAlgCfg(flags, **kwargs):
    #acc=ComponentAccumulator(flags)
    acc=StatisticsAlgBasicCfg(flags, **kwargs)
    acc.addService(histSvc)
    acc.merge(TrackerSpacePoint_OutputCfg(flags))
    return acc
