/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// SpacePointCLASS_DEF.h
//   Header file for class SpacePointCLASS_DEF
///////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////
// Class to contain all the CLASS_DEF for Containers and Collections
///////////////////////////////////////////////////////////////////
// Version 1.0 25/09/2002 Veronique Boisvert
///////////////////////////////////////////////////////////////////

#ifndef TRACKERSPACEPOINT_TRACKERSPACEPOINTCLASS_DEF_H
#define TRACKERSPACEPOINT_TRACKERSPACEPOINTCLASS_DEF_H

// The CLASS_DEF's are now with the class definitions.
#include "TrackerSpacePoint/TrackerSpacePointContainer.h"
#include "TrackerSpacePoint/TrackerSpacePointCollection.h"
#include "TrackerSpacePoint/TrackerSpacePointOverlapCollection.h"

#endif // TRACKERSPACEPOINT_TRACKERSPACEPOINTCLASS_DEF_H
