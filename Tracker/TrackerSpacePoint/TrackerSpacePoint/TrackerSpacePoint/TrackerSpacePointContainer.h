/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// SpacePointContaine.h
//   Header file for class TrackerSpacePointContainer
///////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////
// Container for SpacePoints  collections for InnerDetector and Muons
///////////////////////////////////////////////////////////////////
// Version 1.0 14/10/2003 Veronique Boisvert
///////////////////////////////////////////////////////////////////

#ifndef TRACKERSPACEPOINT_TRACKERSPACEPOINTCONTAINER_H
#define TRACKERSPACEPOINT_TRACKERSPACEPOINTCONTAINER_H

// Base classes
#include "EventContainers/IdentifiableContainer.h"
//Needed Classes
#include "TrackerSpacePoint/TrackerSpacePointCollection.h"
//#include "TrackerSpacePoint/TrackerSpacePointCLASS_DEF.h"


typedef EventContainers::IdentifiableCache< TrackerSpacePointCollection > TrackerSpacePointCache;

class TrackerSpacePointContainer 
: public IdentifiableContainer<TrackerSpacePointCollection>{

  ///////////////////////////////////////////////////////////////////
  // Public methods:
  ///////////////////////////////////////////////////////////////////
public:

  // Constructor with parameters:
  TrackerSpacePointContainer(unsigned int max);
  
  TrackerSpacePointContainer(TrackerSpacePointCache*);

  // Destructor:
  virtual ~TrackerSpacePointContainer();

   /** return class ID */
  static const CLID& classID() 
    {
      static const CLID id = 1273119430 ; 
      return id; 
      // we do not know why using the traits does not work
      //return ClassID_traits<TrackerSpacePointContainer>::ID();
    }

   /** return class ID */
  virtual const CLID& clID() const
    {
      return classID();
    }


  ///////////////////////////////////////////////////////////////////
  // Const methods:
  ///////////////////////////////////////////////////////////////////


  ///////////////////////////////////////////////////////////////////
  // Non-const methods:
  ///////////////////////////////////////////////////////////////////



  ///////////////////////////////////////////////////////////////////
  // Private methods:
  ///////////////////////////////////////////////////////////////////
private:

  TrackerSpacePointContainer() = delete;
  TrackerSpacePointContainer(const TrackerSpacePointContainer&) = delete;
  TrackerSpacePointContainer &operator=(const TrackerSpacePointContainer&) = delete;
  ///////////////////////////////////////////////////////////////////
  // Private data:
  ///////////////////////////////////////////////////////////////////
private:

};
///////////////////////////////////////////////////////////////////
// Inline methods:
///////////////////////////////////////////////////////////////////

#include "AthenaKernel/CLASS_DEF.h"
CLASS_DEF(TrackerSpacePointContainer,1214152083,1)


CLASS_DEF( TrackerSpacePointCache , 205005916, 1 )


#endif // TRACKERSPACEPOINT_SPACEPOINTCONTAINER_H
