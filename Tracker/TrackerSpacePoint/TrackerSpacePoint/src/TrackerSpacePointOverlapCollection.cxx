/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// SpacePointOverlapCollection.cxx
//   Implementation file for class SpacePointOverlapCollection
///////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////
// Version 1.0 14/10/2003 Veronique Boisvert
///////////////////////////////////////////////////////////////////

#include "TrackerSpacePoint/TrackerSpacePointOverlapCollection.h"
// Constructor with parameters:
TrackerSpacePointOverlapCollection::TrackerSpacePointOverlapCollection()

{}

// Destructor:
TrackerSpacePointOverlapCollection::~TrackerSpacePointOverlapCollection()
{ }

