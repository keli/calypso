/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// TrackerSpacePointCollection.cxx
//   Implementation file for class TrackerSpacePointCollection
///////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////
// Version 1.0 14/10/2003 Veronique Boisvert
///////////////////////////////////////////////////////////////////

#include "TrackerSpacePoint/TrackerSpacePointCollection.h"
#include "GaudiKernel/MsgStream.h"
// Constructor with parameters:
TrackerSpacePointCollection::TrackerSpacePointCollection(const IdentifierHash idHash) :
  m_idHash(idHash),
  m_id()
{}

// Constructor with parameters:
TrackerSpacePointCollection::TrackerSpacePointCollection(const TrackerSpacePointCollection& spc) :
  DataVector< TrackerSpacePoint >( spc ),
  m_idHash(),
  m_id()
{
	// added this ctor as a temporary hack for HLT
	// it should never be called.
	throw TrackerSpacePointCollectionCopyConstructorCalled();
}

// Destructor:
TrackerSpacePointCollection::~TrackerSpacePointCollection()
{ }

Identifier TrackerSpacePointCollection::identify() const
{
  return m_id;
}

IdentifierHash TrackerSpacePointCollection::identifyHash() const
{
  return m_idHash;
}

void TrackerSpacePointCollection::setIdentifier(Identifier id)
{
  m_id = id;
}

std::string TrackerSpacePointCollection::type() const
{
  return "TrackerSpacePointCollection";
}
/**Overload of << operator for MsgStream for debug output*/
MsgStream& operator << ( MsgStream& sl, const TrackerSpacePointCollection& coll) {
  sl << "TrackerSpacePointCollection: "
     << "identify()="<< coll.identify()
     << ", SP=[";
  TrackerSpacePointCollection::const_iterator it = coll.begin();
  TrackerSpacePointCollection::const_iterator itEnd = coll.end();
  for (;it!=itEnd;++it) sl<< (**it)<<", ";
  sl <<" ]"<<std::endl;
  return sl;
}

/**Overload of << operator for std::ostream for debug output*/ 
std::ostream& operator << ( std::ostream& sl, const TrackerSpacePointCollection& coll) {
  sl << "TrackerSpacePointCollection: "
     << "identify()="<< coll.identify()
     << ", SP=[";
  TrackerSpacePointCollection::const_iterator it = coll.begin();
  TrackerSpacePointCollection::const_iterator itEnd = coll.end();
  for (;it!=itEnd;++it) sl<< (**it)<<", ";
  sl <<" ]"<<std::endl;
  return sl;
}
