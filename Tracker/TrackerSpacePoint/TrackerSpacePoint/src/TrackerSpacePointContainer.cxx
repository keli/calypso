/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// TrackerSpacePointContainer.cxx
//   Implementation file for class TrackerSpacePointContainer
///////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////
// Version 1.0 14/10/2003 Veronique Boisvert
///////////////////////////////////////////////////////////////////

#include "TrackerSpacePoint/TrackerSpacePointContainer.h"

// Constructor with parameters:
TrackerSpacePointContainer::TrackerSpacePointContainer(unsigned int max) :
  IdentifiableContainer<TrackerSpacePointCollection>(max)
{
}

// Constructor with parameters:
TrackerSpacePointContainer::TrackerSpacePointContainer(TrackerSpacePointCache *cache) :
  IdentifiableContainer<TrackerSpacePointCollection>(cache)
{
}

// Destructor:
TrackerSpacePointContainer::~TrackerSpacePointContainer()
{

}


