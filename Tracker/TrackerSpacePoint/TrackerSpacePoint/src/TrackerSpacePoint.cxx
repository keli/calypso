/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// SpacePoint.cxx Implementation file for class SpacePoint
///////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////
// Version 1.0 14/10/2003 Veronique Boisvert
// Version 2.0   /12/2005 Martin Siebel
///////////////////////////////////////////////////////////////////

#include <new>
#include "TrackerSpacePoint/TrackerSpacePoint.h"
#include "TrackerRawData/TrackerCluster.h"
#include "GaudiKernel/MsgStream.h"
#include "TrkEventPrimitives/LocalParameters.h"
#include "TrkDetElementBase/TrkDetElementBase.h"
#include "TrkSurfaces/Surface.h"
#include <math.h>

  
  // Destructor:
  TrackerSpacePoint::~TrackerSpacePoint()
  {
    delete m_clusList;
  }
  
  // ------------------------------------------------------------------
  
  // Default constructor
  TrackerSpacePoint::TrackerSpacePoint()
    :
    m_clusList(nullptr),
    m_elemIdList(0,0),
    m_position(),
    m_globalCovariance()
  {
  } 
  
  // ------------------------------------------------------------------
  
  // copy constructor
  TrackerSpacePoint::TrackerSpacePoint(const TrackerSpacePoint & SP) :
    Trk::MeasurementBase(SP)
  {
    m_elemIdList = SP.m_elemIdList;
    m_position = SP.m_position;
    if (SP.m_clusList){
      m_clusList = new std::pair<const TrackerCluster*, const TrackerCluster*>(*SP.m_clusList);
    } else {
      m_clusList=nullptr;
    }    
    m_globalCovariance = SP.m_globalCovariance; 
  }
  
  // ------------------------------------------------------------------
  
  //assignment operator
  TrackerSpacePoint& TrackerSpacePoint::operator=(const TrackerSpacePoint& SP)
  {
    if (&SP !=this) 
    {
			Trk::MeasurementBase::operator=(SP);
			delete m_clusList;
			m_elemIdList = SP.m_elemIdList;
			m_position = SP.m_position;
			if (SP.m_clusList){
			  m_clusList = new std::pair<const TrackerCluster*, const TrackerCluster*>(*SP.m_clusList);
			}  else {
        m_clusList=nullptr;
      }  
			m_globalCovariance = SP.m_globalCovariance; 
    }
    return *this;
  }
  
  // ------------------------------------------------------------------
  
  /**Overload of << operator for both, MsgStream and std::ostream for debug output*/ 
  MsgStream& operator << ( MsgStream& sl, const TrackerSpacePoint& spacePoint)
  { 
    return spacePoint.dump(sl);
  }
  
  // ------------------------------------------------------------------
  
  std::ostream& operator << ( std::ostream& sl, const TrackerSpacePoint& spacePoint)
  {
    return spacePoint.dump(sl);
  }

  /** set up the global covariance matrix by rotating the local one */
  
   void TrackerSpacePoint::setupGlobalFromLocalCovariance()
  {
    const Amg::MatrixX& lc = this->localCovariance();

    Amg::MatrixX cov(3,3);

    cov<<
      lc(0,0),lc(0,1),0.,
      lc(1,0),lc(1,1),0.,
      0.     ,0.     ,0.;

    const Amg::RotationMatrix3D& R = associatedSurface().transform().rotation();
    m_globalCovariance = R*cov*R.transpose();
    
  }

  const Trk::Surface& TrackerSpacePoint::associatedSurface() const
    { 
      assert(m_clusList->first->detectorElement()); 
      return m_clusList->first->detectorElement()->surface(); 
    }



