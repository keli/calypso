#!/usr/bin/env python
import sys

def GeoModelTestCfg(flags, name="GeoModelTestAlg", **kwargs):

    from FaserGeoModel.FaserGeoModelConfig import FaserGeometryCfg
    a = FaserGeometryCfg(flags)

    from MagFieldServices.MagFieldServicesConfig import MagneticFieldSvcCfg
    a.merge(MagneticFieldSvcCfg(flags))
    fieldSvc = a.getService("FaserFieldSvc")

    from GeoModelTest.GeoModelTestConf import GeoModelTestAlg
    a.addEventAlgo(GeoModelTestAlg(name, **kwargs))
    a.getEventAlgo(name).FieldService = fieldSvc

    return a


if __name__ == "__main__":
    # from AthenaCommon.Logging import log, logging
    from AthenaCommon.Constants import VERBOSE, INFO
    from AthenaCommon.Configurable import Configurable
    from AthenaConfiguration.ComponentFactory import CompFactory
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags

    Configurable.configurableRun3Behavior = True
    
# Flags for this job
    ConfigFlags.Input.isMC = True                                # Needed to bypass autoconfig
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-XXXX-XXX-XX"          # Needed to bypass autoconfig, only the "OFLCOND" matters at the moment
    ConfigFlags.GeoModel.FaserVersion     = "FASER-00"           # Default FASER geometry
    ConfigFlags.GeoModel.GeoExportFile    = "faserGeo.db"        # Writes out a GeoModel file with the full geometry tree (optional, comment out to skip)
    ConfigFlags.Detector.SimulateVeto     = True
    ConfigFlags.Detector.SimulateTrigger  = True
    ConfigFlags.Detector.SimulatePreshower= True
    ConfigFlags.Detector.SimulateFaserSCT = True
    ConfigFlags.Detector.SimulateUpstreamDipole = True
    ConfigFlags.Detector.SimulateCentralDipole = True
    ConfigFlags.Detector.SimulateDownstreamDipole = True
    ConfigFlags.lock()

# Configure components
    from AthenaConfiguration.MainServicesConfig import MainServicesSerialCfg
    acc = MainServicesSerialCfg()

# Set things up to create a conditions DB with neutral Tracker alignment transforms
    acc.merge(GeoModelTestCfg(ConfigFlags, 
                              AlignDbTool=CompFactory.TrackerAlignDBTool("AlignDbTool", 
                                        OutputTool = CompFactory.AthenaOutputStreamTool("DbStreamTool", 
                                                         OutputFile = "FaserSCT_AlignDb.pool.root"))))
    acc.addService(CompFactory.IOVRegistrationSvc(PayloadTable=False))
    acc.getService("IOVRegistrationSvc").OutputLevel = VERBOSE
    acc.getService("IOVDbSvc").dbConnection = "sqlite://;schema=ALLP200.db;dbname=OFLP200" 

# Configure verbosity    
    # ConfigFlags.dump()
    # logging.getLogger('forcomps').setLevel(VERBOSE)
    # acc.foreach_component("*").OutputLevel = VERBOSE
    # acc.foreach_component("*ClassID*").OutputLevel = INFO
    # log.setLevel(VERBOSE)
    
# Execute and finish
    sys.exit(int(acc.run(maxEvents=1).isFailure()))
