// -*- C++ -*-

/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
   */

#ifndef RECONSTRUCTION_TRUTHTRACKSEEDS_H
#define RECONSTRUCTION_TRUTHTRACKSEEDS_H

#include "StoreGate/ReadCondHandleKey.h"
#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "Identifier/Identifier.h"

#include "GeneratorObjects/McEventCollection.h"
#include "TrackerRawData/FaserSCT_ClusterCollection.h"
#include "TrackerRawData/FaserSCT_ClusterContainer.h"
#include "TrackerRawData/TrackerClusterContainer.h"
#include "TrackerReadoutGeometry/SiDetectorElementCollection.h"
#include "TrackerSpacePoint/TrackerSpacePoint.h"
#include "TrackerSpacePoint/TrackerSpacePointContainer.h"
#include "TrackerSpacePoint/TrackerSpacePointOverlapCollection.h"
#include "TrackerSimEvent/FaserSiHitCollection.h"
#include "TrackerRawData/FaserSCT_RDO_Container.h"
#include "TrackerSimData/TrackerSimDataCollection.h"


#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"

#include <string>
#include <vector>
#include "TH1.h"
#include "TH2.h"

#include <string>

class TrackerSpacePointCollection; 
class TrackerSpacePointOverlapCollection; 
class TrackerSpacePointContainer; 
class FaserSCT_ID;


class TruthTrackSeeds:public AthReentrantAlgorithm {

  public:

    /**
     * @name AthReentrantAlgorithm methods
     */
    TruthTrackSeeds(const std::string& name,
	ISvcLocator* pSvcLocator);

    virtual ~TruthTrackSeeds() = default;

    virtual StatusCode initialize() override;

    virtual StatusCode execute (const EventContext& ctx) const override;

    virtual StatusCode finalize() override;


  private:
    TruthTrackSeeds() = delete;
    TruthTrackSeeds(const TruthTrackSeeds&) =delete;
    TruthTrackSeeds &operator=(const TruthTrackSeeds&) = delete;
    SG::ReadHandleKey<TrackerSpacePointContainer>  m_Sct_spcontainerKey{this, "SpacePointsSCTName", "SCT spContainer"};

    SG::WriteHandleKey<TrackerSpacePointContainer>  m_seed_spcontainerKey{this, "SpacePointsSeedsName", "TrackerSpacePointContainer", "output seeds"};

    SG::ReadCondHandleKey<TrackerDD::SiDetectorElementCollection> m_SCTDetEleCollKey{this, "SCTDetEleCollKey", "SCT_DetectorElementCollection", "Key of SiDetectorElementCollection for SCT"};

    SG::ReadHandleKey<McEventCollection> m_mcEventKey       { this, "McEventCollection", "GEN_EVENT" };
    SG::ReadHandleKey<FaserSiHitCollection> m_faserSiHitKey { this, "FaserSiHitCollection", "SCT_Hits" };
    SG::ReadHandleKey<FaserSCT_RDO_Container> m_faserRdoKey { this, "FaserSCT_RDO_Container", "SCT_RDOs"};
    SG::ReadHandleKey<TrackerSimDataCollection> m_sctMap {this, "TrackerSimDataCollection", "SCT_SDO_Map"};


    const FaserSCT_ID* m_idHelper{nullptr};
    mutable std::atomic<int> m_numberOfEvents{0};
    mutable std::atomic<int> m_numberOfSPCollection{0};
    mutable std::atomic<int> m_numberOfEmptySPCollection{0};
    mutable std::atomic<int> m_numberOfSP{0};
    mutable std::atomic<int> m_numberOfNoMap{0};
    mutable std::atomic<int> m_numberOfHits{0};
    mutable std::atomic<int> m_numberOfMatchSP{0};
    mutable std::atomic<int> m_numberOfFills{0}; 
    TH1* m_hist_x;
    TH1* m_hist_n;
    TH1* m_hist_y;
    TH1* m_hist_z;
    TH1* m_hist_r;
    TH1* m_hist_eta;
    TH1* m_hist_phi;
    TH1* m_hist_sp_station;
    TH1* m_hist_sp_layer;
    TH1* m_hist_sp_plane;
    TH1* m_hist_sp_row;
    TH1* m_hist_sp_module;
    TH1* m_hist_sp_sensor;
    TH1* m_hist_station;
    TH1* m_hist_strip;
    TH1* m_hist_layer;
    TH2* m_hist_x_z;
    TH2* m_hist_y_z;
    TH2* m_hist_x_y_plane0;
    TH2* m_hist_x_y_plane1;
    TH2* m_hist_x_y_plane2;
    TH2* m_hist_x_y_plane3;
    TH2* m_hist_x_y_plane4;
    TH2* m_hist_x_y_plane5;
    TH2* m_hist_x_y_plane6;
    TH2* m_hist_x_y_plane7;
    TH2* m_hist_x_y_plane8;
    ServiceHandle<ITHistSvc>  m_thistSvc;


};


#endif // RECONSTRUCTION_TRUTHTRACKSEEDS_H
