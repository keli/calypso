"""Define methods to construct configured SCT Digitization tools and algorithms

Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
"""
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from TruthTrackSeeds.TruthTrackSeedsConf import TruthTrackSeeds
PileUpXingFolder=CompFactory.PileUpXingFolder

from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
from GaudiSvc.GaudiSvcConf import THistSvc
histSvc= THistSvc()
histSvc.Output += [ "TruthTrackSeeds DATAFILE='truthtrackseeds.root' OPT='RECREATE'" ]


def TruthTrackSeedsBasicCfg(flags, **kwargs):
    """Return ComponentAccumulator for TruthTrackSeeds"""
    acc = ComponentAccumulator()
    kwargs.setdefault("SpacePointsSCTName", "SCT_SpacePointContainer")
    kwargs.setdefault("SpacePointsSeedsName", "Seeds_SpacePointContainer")
    acc.addEventAlgo(TruthTrackSeeds(**kwargs))
    return acc

def TruthTrackSeeds_OutputCfg(flags):
    """Return ComponentAccumulator with Output for SCT. Not standalone."""
    acc = ComponentAccumulator()
#    ItemList = ["FaserSCT_ClusterContainer#*"]
    acc.merge(OutputStreamCfg(flags, "RDO"))
    return acc

def TruthTrackSeedsCfg(flags, **kwargs):
    #acc=ComponentAccumulator(flags)
    acc=TruthTrackSeedsBasicCfg(flags, **kwargs)
    acc.addService(histSvc)
    acc.merge(TruthTrackSeeds_OutputCfg(flags))
    return acc
