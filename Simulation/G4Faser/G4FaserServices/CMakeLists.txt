################################################################################
# Package: G4FaserServices
################################################################################

# Declare the package name:
atlas_subdir( G4FaserServices )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          GaudiKernel
                          PRIVATE
                          Control/AthenaBaseComps
                          Control/StoreGate
                          DetectorDescription/AtlasDetDescr
                          Generators/GeneratorObjects
                          Generators/TruthUtils
                          Simulation/G4Sim/FaserMCTruth
                          Simulation/G4Sim/SimHelpers
                          Simulation/G4Sim/TrackRecord
                          Simulation/ISF/ISF_Core/FaserISF_Event
                          Simulation/ISF/ISF_Core/FaserISF_Interfaces
                          Simulation/ISF/ISF_HepMC/FaserISF_HepMC_Interfaces
                          Tools/PmbCxxUtils )

# External dependencies:
find_package( CLHEP )
find_package( Eigen )
find_package( Geant4 )
find_package( HepMC )
find_package( HepPDT )
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread )

# Component(s) in the package:
#atlas_add_component( G4FaserServices
#                     src/*.cxx
#                     src/components/*.cxx
#                     INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${GEANT4_INCLUDE_DIRS} ${HEPMC_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${HEPPDT_INCLUDE_DIRS}
#                     LINK_LIBRARIES ${ROOT_LIBRARIES} ${GEANT4_LIBRARIES} ${HEPMC_LIBRARIES} ${CLHEP_LIBRARIES} ${HEPPDT_LIBRARIES} GaudiKernel AthenaBaseComps StoreGateLib SGtests GeneratorObjects FaserMCTruth SimHelpers FaserISF_Event FaserISF_Interfaces PmbCxxUtils TruthUtils )


# Install files from the package:
atlas_install_python_modules( python/*.py )
atlas_install_scripts( test/*.py )
