################################################################################
# Package: ScintIdCnv
################################################################################

# Declare the package name:
atlas_subdir( ScintIdCnv )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PRIVATE
                          Control/StoreGate
                          DetectorDescription/DetDescrCnvSvc
                          DetectorDescription/IdDictDetDescr
                          GaudiKernel
                          Scintillator/ScintDetDescr/ScintIdentifier )

# Component(s) in the package:
atlas_add_component( ScintIdCnv
                     src/*.cxx
                     LINK_LIBRARIES StoreGateLib SGtests DetDescrCnvSvcLib IdDictDetDescr GaudiKernel ScintIdentifier )

# Install files from the package:
atlas_install_joboptions( share/*.py )

