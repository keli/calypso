################################################################################
# Package: TriggerGeoModel
################################################################################

# Declare the package name:
atlas_subdir( TriggerGeoModel )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthenaKernel
                          Database/RDBAccessSvc
                          DetectorDescription/GeoModel/GeoModelUtilities
                          DetectorDescription/GeoModel/GeoModelFaserUtilities
			              DetectorDescription/GeoPrimitives
                          GaudiKernel
                          Scintillator/ScintDetDescr/ScintGeoModelUtils
                          Scintillator/ScintDetDescr/ScintReadoutGeometry
                          PRIVATE
                          Control/SGTools
                          Control/StoreGate
                          Database/AthenaPOOL/AthenaPoolUtilities
                          DetectorDescription/DetDescrCond/DetDescrConditions
                          DetectorDescription/GeoModel/GeoModelInterfaces
                          DetectorDescription/GeometryDBSvc
                          DetectorDescription/Identifier
                          Scintillator/ScintDetDescr/ScintIdentifier 
                          DetectorDescription/FaserDetDescr
                          Scintillator/ScintDetDescr/ScintIdentifier )

# External dependencies:
find_package( Boost COMPONENTS filesystem thread system )
find_package( CORAL COMPONENTS CoralBase CoralKernel RelationalAccess )
find_package( Eigen )
find_package( GeoModelCore )

# Component(s) in the package:
atlas_add_component( TriggerGeoModel
        		     src/*.cxx
                     src/components/*.cxx
                     INCLUDE_DIRS ${Boost_INCLUDE_DIRS} ${CORAL_INCLUDE_DIRS} 
                     LINK_LIBRARIES ${Boost_LIBRARIES} ${CORAL_LIBRARIES} ${GEOMODEL_LIBRARIES} AthenaKernel GeoModelUtilities GeoModelFaserUtilities GaudiKernel SGTools StoreGateLib SGtests AthenaPoolUtilities DetDescrConditions FaserDetDescr ScintGeoModelUtils ScintReadoutGeometry ScintIdentifier Identifier )

#atlas_add_test( SCT_GMConfig_test
#                SCRIPT test/SCT_GMConfig_test.py
#                PROPERTIES TIMEOUT 300 )

# Install files from the package:
atlas_install_python_modules( python/*.py )
atlas_install_scripts( test/*.py )
