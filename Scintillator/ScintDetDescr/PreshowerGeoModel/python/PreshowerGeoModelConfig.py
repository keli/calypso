# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from AthenaCommon import CfgMgr

def getPreshowerDetectorTool(name="PreshowerDetectorTool", **kwargs):
    kwargs.setdefault("DetectorName",     "Preshower");
    kwargs.setdefault("Alignable",        True);
    kwargs.setdefault("RDBAccessSvc",     "RDBAccessSvc");
    kwargs.setdefault("GeometryDBSvc",    "ScintGeometryDBSvc");
    kwargs.setdefault("GeoDbTagSvc",      "GeoDbTagSvc");
    return CfgMgr.PreshowerDetectorTool(name, **kwargs)

from IOVDbSvc.IOVDbSvcConfig import addFoldersSplitOnline

def PreshowerGeometryCfg( flags ):
    from FaserGeoModel.GeoModelConfig import GeoModelCfg
    acc = GeoModelCfg( flags )
    geoModelSvc = acc.getPrimary()

    from GeometryDBSvc.GeometryDBSvcConf import GeometryDBSvc
    acc.addService(GeometryDBSvc("ScintGeometryDBSvc"))

    from RDBAccessSvc.RDBAccessSvcConf import RDBAccessSvc
    acc.addService(RDBAccessSvc("RDBAccessSvc"))

    from DBReplicaSvc.DBReplicaSvcConf import DBReplicaSvc
    acc.addService(DBReplicaSvc("DBReplicaSvc"))

    from PreshowerGeoModel.PreshowerGeoModelConf import PreshowerDetectorTool
    preshowerDetectorTool = PreshowerDetectorTool()

    preshowerDetectorTool.useDynamicAlignFolders = flags.GeoModel.Align.Dynamic
    geoModelSvc.DetectorTools += [ preshowerDetectorTool ]

    # if flags.GeoModel.Align.Dynamic:
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL1/ID","/Indet/AlignL1/ID",className="CondAttrListCollection"))
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL2/SCT","/Indet/AlignL2/SCT",className="CondAttrListCollection"))
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL3","/Indet/AlignL3",className="AlignableTransformContainer"))
    # else:
    #     if (not flags.Detector.SimulatePreshower) or flags.Detector.OverlayPreshower:
    #         acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/Align","/Indet/Align",className="AlignableTransformContainer"))
    #     else:
    #         acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/Align","/Indet/Align"))

    # if flags.Common.Project is not "AthSimulation": # Protection for AthSimulation builds
    #     if (not flags.Detector.SimulatePreshower) or flags.Detector.OverlayPreshower:
    #         from PreshowerConditionsAlgorithms.PreshowerConditionsAlgorithmsConf import PreshowerAlignCondAlg
    #         preshowerAlignCondAlg = PreshowerAlignCondAlg(name = "PreshowerAlignCondAlg",
    #                                             UseDynamicAlignFolders = flags.GeoModel.Align.Dynamic)
    #         acc.addCondAlgo(preshowerAlignCondAlg)
    #         from PreshowerConditionsAlgorithms.PreshowerConditionsAlgorithmsConf import PreshowerDetectorElementCondAlg
    #         preshowerDetectorElementCondAlg = PreshowerDetectorElementCondAlg(name = "PreshowerDetectorElementCondAlg")
    #         acc.addCondAlgo(preshowerDetectorElementCondAlg)

    return acc
