# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from AthenaCommon import CfgMgr

def getVetoDetectorTool(name="VetoDetectorTool", **kwargs):
    kwargs.setdefault("DetectorName",     "Veto");
    kwargs.setdefault("Alignable",        True);
    kwargs.setdefault("RDBAccessSvc",     "RDBAccessSvc");
    kwargs.setdefault("GeometryDBSvc",    "ScintGeometryDBSvc");
    kwargs.setdefault("GeoDbTagSvc",      "GeoDbTagSvc");
    return CfgMgr.VetoDetectorTool(name, **kwargs)

from IOVDbSvc.IOVDbSvcConfig import addFoldersSplitOnline

def VetoGeometryCfg( flags ):
    from FaserGeoModel.GeoModelConfig import GeoModelCfg
    acc = GeoModelCfg( flags )
    geoModelSvc = acc.getPrimary()

    from GeometryDBSvc.GeometryDBSvcConf import GeometryDBSvc
    acc.addService(GeometryDBSvc("ScintGeometryDBSvc"))

    from RDBAccessSvc.RDBAccessSvcConf import RDBAccessSvc
    acc.addService(RDBAccessSvc("RDBAccessSvc"))

    from DBReplicaSvc.DBReplicaSvcConf import DBReplicaSvc
    acc.addService(DBReplicaSvc("DBReplicaSvc"))

    from VetoGeoModel.VetoGeoModelConf import VetoDetectorTool
    vetoDetectorTool = VetoDetectorTool()

    vetoDetectorTool.useDynamicAlignFolders = flags.GeoModel.Align.Dynamic
    geoModelSvc.DetectorTools += [ vetoDetectorTool ]

    # if flags.GeoModel.Align.Dynamic:
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL1/ID","/Indet/AlignL1/ID",className="CondAttrListCollection"))
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL2/SCT","/Indet/AlignL2/SCT",className="CondAttrListCollection"))
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL3","/Indet/AlignL3",className="AlignableTransformContainer"))
    # else:
    #     if (not flags.Detector.SimulateVeto) or flags.Detector.OverlayVeto:
    #         acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/Align","/Indet/Align",className="AlignableTransformContainer"))
    #     else:
    #         acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/Align","/Indet/Align"))

    # if flags.Common.Project is not "AthSimulation": # Protection for AthSimulation builds
    #     if (not flags.Detector.SimulateVeto) or flags.Detector.OverlayVeto:
    #         from VetoConditionsAlgorithms.VetoConditionsAlgorithmsConf import VetoAlignCondAlg
    #         vetoAlignCondAlg = VetoAlignCondAlg(name = "VetoAlignCondAlg",
    #                                             UseDynamicAlignFolders = flags.GeoModel.Align.Dynamic)
    #         acc.addCondAlgo(vetoAlignCondAlg)
    #         from VetoConditionsAlgorithms.VetoConditionsAlgorithmsConf import VetoDetectorElementCondAlg
    #         vetoDetectorElementCondAlg = VetoDetectorElementCondAlg(name = "VetoDetectorElementCondAlg")
    #         acc.addCondAlgo(vetoDetectorElementCondAlg)

    return acc
