# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from ISF_Algorithms.collection_merger_helpersNew import CollectionMergerCfg

# from VetoG4_SD.VetoG4_SDConf import VetoSensorSDTool
from VetoG4_SD.VetoG4_SDConfig import getVetoSensorSD

def VetoSensorSDCfg(ConfigFlags, name="VetoSensorSD", **kwargs):

    result = ComponentAccumulator()
    bare_collection_name = "VetoHits"
    mergeable_collection_suffix = "_G4"
    merger_input_property = "VetoHits"

    acc, hits_collection_name = CollectionMergerCfg(ConfigFlags, bare_collection_name, mergeable_collection_suffix, merger_input_property)
    kwargs.setdefault("LogicalVolumeNames", ["Veto::Plate"])
    kwargs.setdefault("OutputCollectionNames", [hits_collection_name])

    result.merge(acc)
    return result, getVetoSensorSD(name, **kwargs)
