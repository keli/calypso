/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// FaserDetTechnology.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#ifndef FASERDETDESCR_FASERDETTECHNOLOGY_H
#define FASERDETDESCR_FASERDETTECHNOLOGY_H 1

#include <assert.h>

// use these macros to check a given FaserDetTechnology for its validity

#define validFaserDetTechnology(detTechn) ( (detTechn<FaserDetDescr::fNumFaserDetTechnologies) && (detTechn>=FaserDetDescr::fFirstFaserDetTechnology) )
#define assertFaserDetTechnology(detTechn) ( assert(validFaserDetTechnology(detTechn)) )

namespace FaserDetDescr {

 /** @enum FaserDetTechnology
   
     A simple enum of FASER detector technologies.

   */

   enum FaserDetTechnology {       
        // Unset
            fUndefined                  = 0,
        // first Geometry element in enum, used in e.g. loops
            fFirstFaserDetTechnology    = 1,
        // Scintillator
            fFirstFaserScintTechnology  = 1,
            fFaserVeto                  = 1,
            fFaserTrigger               = 2,
            fFaserPreshower             = 3,
            fLastFaserScintTechnology   = 3,
        // Tracker
            fFirstFaserTrackerTechnology = 4,
            fFaserSCT                    = 4,
            fLastFaserTrackerTechnology  = 4,
        // Calorimeter
            fFirstFaserCaloTechnology    = 5,
            fFaserECAL                   = 5,
            fLastFaserCaloTechnology     = 5,
        // number of defined detector technologies
            fNumFaserDetTechnologies    = 6
   };

} // end of namespace

#endif // FASERDETDESCR_FASERDETTECHNOLOGY
