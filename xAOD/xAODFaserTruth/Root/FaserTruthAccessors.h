// -*- C++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: TruthAccessors_v1.h 623284 2014-10-22 14:07:48Z krasznaa $
#ifndef XAODTRUTH_TRUTHACCESSORS_H
#define XAODTRUTH_TRUTHACCESSORS_H

// EDM include(s):
#include "AthContainers/AuxElement.h"

// Local include(s):
#include "xAODFaserTruth/FaserTruthEvent.h"
#include "xAODFaserTruth/FaserTruthParticle.h"

namespace xAOD {

   /// This function holds on to Accessor objects that can be used by each
   /// FaserTruthParticle object at runtime to get/set parameter values on
   /// themselves.
   SG::AuxElement::Accessor< float >*
   polarizationAccessor( FaserTruthParticle::PolParam type );

   /// Helper function for getting accessors for integer type PDF information
   SG::AuxElement::Accessor< int >*
   pdfInfoAccessorInt( FaserTruthEvent::PdfParam type );

   /// Helper function for getting accessors for floating point PDF information
   SG::AuxElement::Accessor< float >*
   pdfInfoAccessorFloat( FaserTruthEvent::PdfParam type );

} // namespace xAOD

#endif // XAODTRUTH_TRUTHACCESSORS_H
